Git Rebase (A walkthrough in rebasing and merge conflicts).
===========================================================

1) Create a repo on git, say, TestRepo
2) Clone the repo onto your computer.
3) Create a README.md file and add the following line 'This is a test of the readme file'.
4) Add the README.md file to be tracked by git using 'git add .'
5) Commit the changes with 'git commit -am "My commit message"'
6) Create a new branch with 'git branch NameOfBranch'
7) Move onto the new branch with 'git checkout NameOfBranch'
8) Make changes to the README.md file on the same line as in step 3.
9) Commit changes on your new branch with 'git commit -am "My commit message"'
10) Set the upstreatm for your branch with 'git push --set-upstream origin NameOfBranch'
11) Push your commited changes with 'git push NameOfBranch'
12) Now, let's rebase these changes, creating the rebase conflict since our error was on the same line. First, switch back to your master branch with 'git checkout master'
13) Make another change on the same line as in steps 3 and 8.
14) Commit these changes and push. 
15) Try to rebase with 'git rebase NameOfBranch'
16) An error should be evoked. Check out the status of git with 'git status'
17) Look at the differences between the branches with 'git diff'
18) Merge the two by hand so that there aren't any conflicts.
19) Mark the file as resolved with 'git add readme.md'
20) Continue the rebase with 'git rebase --continue'
21) Commit and Push the new rebased version of the master branch to git
22) Delete the feature branch with 'git -d NameOfBranch'
23) Done! Now you've managed your first merge conflict.
